﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dzien5
{
    class Program
    {
        static async Task Main(string[] args)
        {
            //#region async
            //await AwaitedMethod();
            //Console.WriteLine("After async");
            //#endregion

            //Console.ReadLine();
            //throw new NotImplementedException("Ta metoda jest niezaimplementowana. Nie używać.");
            //#region wyjątki

            
            ////Console.WriteLine("Podaj swój wiek: ");
            
            ////int? age = null;
            ////int tries = 0;
            ////while (age == null)
            ////{
            ////    try
            ////    {
            ////        string input = Console.ReadLine();
            ////        age = int.Parse(input);
            ////    }
            ////    catch (OverflowException) when(tries < 5)
            ////    {
            ////        tries++;
            ////        Console.WriteLine("Ta jasne! Niemożliwe, żebyś miał/miała tyle lat!");
            ////    }
            ////    catch (FormatException)
            ////    {
            ////        Console.WriteLine("Sorry! Podaj tylko wiek w latach!");
            ////    }
            ////}

            ////catch (Exception)
            ////{
            ////    Console.WriteLine("Przepraszamy. Wystąpił błąd!");
            ////}

            //Console.WriteLine("Ciąg dalszy....");
            ////try
            ////{
            ////    throw new CustomException();
            ////}
            ////catch (Exception e)
            ////{
            ////    Console.WriteLine(e);
            ////}

            ////using (var f = new FileStream(@"c:\temp\log.txt", FileMode.Open))
            ////{

            ////}

            //FileStream f = null;
            //try
            //{
            //    Console.WriteLine("Podaj nazwę pliku: ");
            //    string fileName = Console.ReadLine();
            //    f = new FileStream(fileName, FileMode.Open);
            //    Console.WriteLine("Udało się plik odczytać!");
            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine("Wystąpił błąd!");
            //    throw;
            //}
            //finally
            //{
            //    Console.WriteLine("Zamykanie pliku!");
            //    if (f != null)
            //        f.Dispose();
            //}

            //Console.WriteLine("Asasdasdad");
            //#endregion
            
            //Zadanie: przerobić program czytający ExchangeRate i korzystający z exceptions
            //Zadanie: 
            var t = AwaitedMethodAsync();

            Console.WriteLine("After async call.");

            var result = await t;
            Console.WriteLine($"Answer is : {result}");
        }

        static async Task<int> AwaitedMethodAsync()
        {
            await Task.Delay(3000);
            Console.WriteLine("After delay");
            return 42;
        }
    }
}
