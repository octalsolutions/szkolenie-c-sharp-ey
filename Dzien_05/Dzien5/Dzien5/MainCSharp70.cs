﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Dzien5
{
    class MainCSharp70
    {
        public void Entry()
        {
            OutVariables();
            PatterMatching();
            IsExpression();
            Tuples();
            LocalMethods();
            Literals();
            ExpressionBodiedFunction();
            ThrowExpression(null);
        }
        //(+)out variables
        //)+)pattern matching
        //(+)is-expression
        //(+)switch with patterns
        //(+)tuples + dekonstruction
        //(+)local functions
        //(+)literals
        //ref returns, ref locals
        //(+)expresion bodied functions
        //(+)throw expression
        public void PatterMatching()
        {
            object obj = 12;
            switch (obj)
            {
                case int i:
                    Console.WriteLine("We got an int: {0}", i);
                    break;
                case double d:
                    Console.WriteLine("We got na double: {0}", d);
                    break;
            }
        }
        public void Tuples()
        {
            var (x, y) = (12, 13);
        }
        public void IsExpression()
        {
            int year = 2018;
            if (year is 2018)
            {
                Console.WriteLine("Today");
            }
        }
        public void OutVariables()
        {
            //before:
            int year;
            int.TryParse("1982", out year);
            //now:
            int.TryParse("1982", out int y);
            Console.WriteLine("Rok: {0}", y);
        }
        public void LocalMethods()
        {
            void log(string s) => Console.Write(s); 

            log("Internal method");
            log("Better");
        }
        public void Literals()
        {
            int binary = 0b11110000;
            int huge_number = 0x1234_5678;
            Console.WriteLine("Binary: {0} oraz huge: {1}", binary, huge_number);
        }

        public void ExpressionBodiedFunction() => Console.WriteLine("Shorter form for small method");
        public void ThrowExpression(object refThatCanBeNull)
        {
            //old:
            if (refThatCanBeNull == null)
            {
                throw new NullReferenceException();
            }

            var obj = refThatCanBeNull;
            //new:
            var newObj = refThatCanBeNull ?? throw new NullReferenceException();
        }

        //Zadanie: poćwiczyć ;)
    }
}
