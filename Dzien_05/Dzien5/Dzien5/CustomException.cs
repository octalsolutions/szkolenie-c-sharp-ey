﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dzien5
{
    class ValidationRuleException : Exception
    {
        public ValidationRuleException()
        {
        }

        public ValidationRuleException(string message) : base(message)
        {
        }

        public ValidationRuleException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ValidationRuleException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }

    //Zadanie: napisać program, który wczyta od użytkownika input i spróbuje go zaminić na liczbę. W jakis sposób możemy zabezpieczyć się przed nieprawidłowymi danymi?
    //Zadanie: kiedy ma sens tworzenie własnych wyjątków? A kiedy powinniśmy korzystać ze standardowych?
}
