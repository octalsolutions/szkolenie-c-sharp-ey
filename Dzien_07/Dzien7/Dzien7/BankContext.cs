﻿using System.Data.Entity;
using Dzien7.Entities;

namespace Dzien7
{
    class BankContext : DbContext
    {
        public BankContext() : base("DefaultDbContext") {}
        public DbSet<BankAccount> Accounts { get; set; }

        public DbSet<AccountHolder> Holders { get; set; }
    }
}
