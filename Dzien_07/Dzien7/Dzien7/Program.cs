﻿using Dzien7.Entities;

namespace Dzien7
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var context = new BankContext())
            {
                context.Accounts.Add(new BankAccount {Amount = 1, IBAN = "1234"});
                context.SaveChanges();
            }
        }

        //Zadanie: rozbuduj model o Transakcje, stworz migracje i zaaplikują ją na swojej bazie danych. 
        //Zadanie: zmień, któryś z istniejących modeli - zobacz jakie migracje zostaną dodane?
    }
}
