﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dzien7.Entities
{
    public class BankAccount
    {
        [Key]
        public string IBAN { get; set; }
        public AccountHolder Holder { get; set; }
        public decimal Amount { get; set; }
    }
}
