﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Dzien4
{
    class Program
    {
        static void Main(string[] args)
        {
            //#region lambda

            ////Action<string> log = delegate(string s) { Console.WriteLine(s); };
            
            //Action<string> log = s => Console.WriteLine(s);
            //log("First entry");

            //log("Second entry");

            //log("Third entry");

            Func<int, bool> isOdd = (i) => i % 2 == 1;

            //log("5 is " + isOdd(5, 6));

            ////Zadanie: Przepisać kalkulator, który będzie korzystał z lambd w celu obliczania wyrażeń
            ////Zadanie: Użyć lambd do napisania filtrów dla ExchangeRate z wczoraj
            //#endregion

            //#region LINQ

            ////foreach (var price in Enumerable.Range(1, 100))
            ////{
            ////    Console.WriteLine("Price with VAT is {0}", price * 1.23);
            ////}

            ////Console.ReadLine();
            ////var prices = new List<int> {20, 33, 58, 99};
            ////var pricesWithVat = prices.Select(x => x * 1.23);
            ////Array.ForEach(pricesWithVat.ToArray(), x => Console.WriteLine($"Price with VAT: {x}"));


            ////Array.ForEach(new List<int> {20, 33, 58, 99}.Select(x => x * 1.23).ToArray(),
            ////    x => Console.WriteLine($"Price with Vat: {x}"));

            ////List<int> collection = new List<int>();
            ////Func<int, bool> isOdd = (i) => i % 2 == 1;
            ////foreach (var item in Enumerable.Range(1,1000))
            ////{
            ////    if (isOdd(item))
            ////        collection.Add(item);
            ////}

            //// var selectedCustomers = dbContext.Customer.Where(c => c.BirthDate == DateTime.Now);
            //var collection = (from c in new List<Customer>()
            //    let p = c.FirstName.ToUpper()
            //    where p.StartsWith("A")
            //    select new {c.FirstName, c.LastName});
                              
            //var collection2 = new List<Customer>()
            //    .Select(x=>new {p=x.FirstName.ToUpper(), x})
            //    .Where(x => x.p.StartsWith("A"))
            //    .Select(c => new { c.x.FirstName, c.x.LastName });
            //////var sum = collection2.Sum();
            ////Array.ForEach(collection2.ToArray(), c=>Console.WriteLine(c.FirstName));
            ////same as above

            ////string result = Enumerable.Range(1, 100).Select(x =>
            ////{
            ////    if (x % 15 == 0) return "FizzBuzz";
            ////    if (x % 5 == 0) return "Buzz";
            ////    if (x % 3 == 0) return "Fizz";
            ////    return x.ToString();
            ////}).;
            ////Console.WriteLine(result);
            ////Zadanie: Napisać program, który dla listy wartości od 1 do 100, wypisze FizzBuzz za pomocą linq
            ////Zadanie: napisać program, który na podstawie pliku https://archive.ics.uci.edu/ml/datasets/SMS+Spam+Collection stworzy kolekcję zdań przypisanych do kategorii Ham lub Spam
            ////Zadanie: przerobić program, który z kolekcji ExchangeRate stworzy nową kolekcję w której będą tylko elementy spełniające filtr
            //#endregion

            #region events
            FoundOddNumber += Program_FoundOddNumber;

            var handlerOddNumbers = new OddNumberHandler();
            FoundOddNumber += handlerOddNumbers.HandleOddNumber;
            FoundOddNumber += (sender, i) => { File.WriteAllText(@"c:\temp\oddnumbers.txt", i.ToString()); };
            foreach (var item in Enumerable.Range(1, 100))
            {
                if (isOdd(item))
                {
                    FoundOddNumber(null, item);
                }

            }

            FoundOddNumber -= Program_FoundOddNumber;
            //Zadanie: napisać FizzBuzz za pomocą eventów. Zdefiniować event Fizz oraz Buzz i odpowiednio je aktywować gdy odpowiednie warunki zostaną spełnione
            //Zadanie: przerobić program ExcahngeRate, który użyje eventów do zasygnalizowania informacji o tym dany ExchangeRate spełnia jakiś warunek
            #endregion
        }
        public static event EventHandler<int> FoundOddNumber;
        private static void Program_FoundOddNumber(object sender, int e)
        {
            Console.WriteLine($"Found odd number: {e}");
        }

        static private void M1(Action<string> a)
        {

        }




    }
}
