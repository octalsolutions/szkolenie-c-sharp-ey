﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Xml;

namespace MyFirstWebApplication.Models
{
    public class ExchangeRatesProvider
    {
        private static List<ExchangeRate<string>> _cache;
        public List<ExchangeRate<string>> CreateExchangeRates()
        {
            if (_cache == null)
            {
                var exchangeRates = new List<ExchangeRate<string>>();
                XmlReader r = XmlReader.Create(@"https://www.nbp.pl/kursy/xml/a194z181005.xml");
                using (r)

                {
                    while (r.Read())
                    {
                        if (r.Name == "pozycja" && r.IsStartElement())
                        {
                            r.ReadToFollowing("nazwa_waluty");
                            var currencyName = r.ReadElementContentAsString();
                            r.ReadToFollowing("przelicznik");
                            var multiplier = r.ReadElementContentAsObject();
                            r.ReadToFollowing("kod_waluty");
                            var currencyType = r.ReadElementContentAsString();
                            r.ReadToFollowing("kurs_sredni");
                            decimal rate = decimal.Parse((r.ReadElementContentAsObject()).ToString());

                            ExchangeRate<string> ex = new ExchangeRate<string>(currencyName,
                                int.Parse((multiplier).ToString()), currencyType, rate);
                            exchangeRates.Add(ex);
                        }
                    }
                }

                _cache = exchangeRates;
            }

            return _cache;
        }

        public void AddNew(ExchangeRate<string> newRate)
        {
            _cache.Add(newRate);
        }
    }
}