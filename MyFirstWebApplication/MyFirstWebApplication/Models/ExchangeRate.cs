﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace MyFirstWebApplication.Models
{
    public class ExchangeRate<T>

    {
        public ExchangeRate(string currencyName, int multiplier, T code, decimal rate)
        {
            //if (rate < 0.1m)
            //    throw new VeryLowRateException(currencyName);

            CurrencyName = currencyName;
            Multiplier = multiplier;
            CurrencyCode = code;
            Rate = rate;
        }


        public string CurrencyName { get; private set; }


        public int Multiplier { get; private set; }


        public T CurrencyCode { get; private set; }

  
        public decimal Rate { get; private set; }

        public override string ToString()
        {
            return string.Format("Waluta: {0}, Kod waluty: {1}, Kurs wymiany: {2:F3}, Multiplier: {3}", CurrencyName, CurrencyCode, Rate, Multiplier);
        }
    }
}