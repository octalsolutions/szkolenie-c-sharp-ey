﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyFirstWebApplication.Models;
using MyFirstWebApplication.ViewModels;

namespace MyFirstWebApplication.Controllers
{
    public class ExchangeRatesController : Controller
    {
        // GET: ExchangeRates
        public ActionResult Index()
        {
            var provider = new ExchangeRatesProvider();
            var exchangeRates = provider.CreateExchangeRates();
            return View(exchangeRates);
        }

        public ActionResult Details(string currencyCode)
        {
            if (string.IsNullOrWhiteSpace(currencyCode))
                return View();

            var provider = new ExchangeRatesProvider();
            var exchangeRate = provider.CreateExchangeRates().FirstOrDefault(x=>x.CurrencyCode == currencyCode);

            if (exchangeRate == null)
                return View();

            return View(exchangeRate);
        }

        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(NewExchangeRateViewModel newRate)
        {
            if (ModelState.IsValid == false)
                return View(newRate);

            var provider = new ExchangeRatesProvider();
            provider.AddNew(new ExchangeRate<string>(newRate.CurrencyName, 
                                                     newRate.Multiplier, 
                                                     newRate.CurrencyCode,
                                                     (decimal)newRate.Rate));
            return RedirectToAction("Index");
        }
    }
}