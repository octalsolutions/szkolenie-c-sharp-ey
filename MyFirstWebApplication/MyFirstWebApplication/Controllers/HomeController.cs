﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyFirstWebApplication.Controllers
{
    public class HomeController : Controller
    {
        public string Message { get; set; }
        public ActionResult Index()
        {
            Message = "Index";
            var model = new int[] {1, 3, 5, 7}.ToList();
            return View(model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.!" + Message;

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult NewAction(int id)
        {
            return View(id);
        }
    }
}