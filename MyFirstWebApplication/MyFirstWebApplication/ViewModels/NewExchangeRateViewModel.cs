﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace MyFirstWebApplication.ViewModels
{
    public class NewExchangeRateViewModel
    {
        [DisplayName("Nazwa waluty")]
        public string CurrencyName { get; set; }
        [DisplayName("Kod waluty")]
        public string CurrencyCode { get; set; }
        [DisplayName("Kurs wymiany")]
        public double Rate {get; set; }

        [DisplayName("Mnożnik")]
        public int Multiplier { get; set; }
    }
}