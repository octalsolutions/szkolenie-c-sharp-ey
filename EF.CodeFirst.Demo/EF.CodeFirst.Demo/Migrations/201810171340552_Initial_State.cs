namespace EF.CodeFirst.Demo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial_State : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BankAccounts",
                c => new
                    {
                        Number = c.String(nullable: false, maxLength: 128),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Owner_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Number)
                .ForeignKey("dbo.AccountOwners", t => t.Owner_Id)
                .Index(t => t.Owner_Id);
            
            CreateTable(
                "dbo.AccountOwners",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BankAccounts", "Owner_Id", "dbo.AccountOwners");
            DropIndex("dbo.BankAccounts", new[] { "Owner_Id" });
            DropTable("dbo.AccountOwners");
            DropTable("dbo.BankAccounts");
        }
    }
}
