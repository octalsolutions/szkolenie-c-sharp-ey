namespace EF.CodeFirst.Demo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ConstraintOnLastName : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AccountOwners", "LastName", c => c.String(maxLength: 200));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AccountOwners", "LastName", c => c.String());
        }
    }
}
