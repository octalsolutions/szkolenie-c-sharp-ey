﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EF.CodeFirst.Demo.Entities;

namespace EF.CodeFirst.Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var dbContext = new BankDataContext())
            {
                var owner = dbContext.Owners.First();
                foreach (var account in owner.Accounts)
                {
                    Console.WriteLine($"Account no: {account.Number}");
                }
                //foreach (var owner in dbContext.Owners)
                //{
                //    Console.WriteLine($"Owner: {owner.FirstName} {owner.LastName}");
                //}
                //Console.WriteLine("");
                //var owner = new AccountOwner
                //{
                //    FirstName = "Paweł", LastName = "Łukasik"
                //};
                //var bankAccount = new BankAccount
                //{
                //    Amount = 12M, Number = "PL12123333223322", Owner = owner
                //};
                //dbContext.Owners.Add(owner);
                //dbContext.Accounts.Add(bankAccount);
                //dbContext.SaveChanges();
            }
        }
    }
}
