﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF.CodeFirst.Demo.Entities
{
    public class BankAccount
    {
        [Key]
        public string Number { get; set; }
        public decimal Amount { get; set; }
        public AccountOwner Owner { get; set; }
    }
}
