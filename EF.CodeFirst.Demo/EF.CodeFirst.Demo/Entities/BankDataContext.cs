﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF.CodeFirst.Demo.Entities
{
    public class BankDataContext : DbContext
    {
        public BankDataContext() : base("MyConnectionString") {}
        public DbSet<BankAccount> Accounts { get; set; }
        public DbSet<AccountOwner> Owners { get; set; }
    }
}
