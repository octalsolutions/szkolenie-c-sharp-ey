﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF.CodeFirst.Demo.Entities
{
    public class AccountOwner
    {
        [Key]
        public int Id { get; set; }
        public string FirstName { get; set; }
        [MaxLength(200)]
        public string LastName { get; set; }
        
        public virtual List<BankAccount> Accounts { get; set; }
    }
}
