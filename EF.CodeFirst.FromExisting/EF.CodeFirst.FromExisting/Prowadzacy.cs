namespace EF.CodeFirst.FromExisting
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Prowadzacy")]
    public partial class Prowadzacy
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Prowadzacy()
        {
            ProwadzacyKierunek = new HashSet<ProwadzacyKierunek>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(50)][Column("Imie")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(50)]
        public string Nazwisko { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProwadzacyKierunek> ProwadzacyKierunek { get; set; }
    }
}
