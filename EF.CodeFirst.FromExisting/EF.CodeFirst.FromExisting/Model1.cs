namespace EF.CodeFirst.FromExisting
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model1 : DbContext
    {
        public Model1()
            : base("name=Model1")
        {
        }

        public virtual DbSet<Kierunki> Kierunki { get; set; }
        public virtual DbSet<Prowadzacy> Prowadzacy { get; set; }
        public virtual DbSet<ProwadzacyKierunek> ProwadzacyKierunek { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Kierunki>()
                .HasMany(e => e.ProwadzacyKierunek)
                .WithOptional(e => e.Kierunki)
                .HasForeignKey(e => e.IdKierunku);

            modelBuilder.Entity<Prowadzacy>()
                .HasMany(e => e.ProwadzacyKierunek)
                .WithOptional(e => e.Prowadzacy)
                .HasForeignKey(e => e.IdProwadzacego);
        }
    }
}
