namespace EF.CodeFirst.FromExisting
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProwadzacyKierunek")]
    public partial class ProwadzacyKierunek
    {
        public int Id { get; set; }

        public int? IdProwadzacego { get; set; }

        public int? IdKierunku { get; set; }

        public virtual Kierunki Kierunki { get; set; }

        public virtual Prowadzacy Prowadzacy { get; set; }
    }
}
