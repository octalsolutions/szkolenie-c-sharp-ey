namespace EF.CodeFirst.FromExisting
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Kierunki")]
    public partial class Kierunki
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Kierunki()
        {
            ProwadzacyKierunek = new HashSet<ProwadzacyKierunek>();
        }

        public int Id { get; set; }

        [StringLength(255)]
        public string Nazwa { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProwadzacyKierunek> ProwadzacyKierunek { get; set; }
    }
}
