﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class GenericRepository<T>
    {
        private Dictionary<int, T> _dict;

        public GenericRepository()
        {
            _dict = new Dictionary<int, T>();
        }
        public T GetById(int id)
        {
            return _dict[id];
        }

        public List<T> GetAll()
        {
            return _dict.Values.ToList();
        }

        public void Insert(int id, T item)
        {
            _dict.Add(id, item);
        }

        public void Delete(int id)
        {
            _dict.Remove(id);
        }
    }
}
