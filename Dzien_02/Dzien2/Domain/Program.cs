﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    class Program
    {
        static void Main(string[] args)
        {
            var productRepository = new GenericRepository<Product>();
            productRepository.GetAll();

            var usersRepository = new GenericRepository<User>();
            usersRepository.GetAll();

            List<object> list = new List<object>();
            list.Add(12);
            list.Add("asasas");
        }
    }
}
