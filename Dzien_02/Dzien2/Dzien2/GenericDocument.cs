﻿namespace Dzien2
{
    public class GenericDocument<T> where T: IBillingDocument, new()
    {
        public GenericDocument()
        {
            DocumentType = default(T);
        }
        public GenericDocument(T type)
        {
            DocumentType = type;
        }

        public void Issue(T input)
        {

        }
        public T DocumentType { get; set; }
        public decimal Amount { get; set; }

    }
}
