﻿namespace Dzien2
{
    public enum DocumentType
    {
        Invoice,
        Order,
        Receipt
    }
    public interface IBillingDocument
    {
        int Type { get; }
        decimal Amount { get; set; }

        void SetAmount(decimal amount);
        void Issue();
    }
}
