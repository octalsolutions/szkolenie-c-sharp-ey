﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Dzien2
{
    class Program
    {
        private const int Type = 0;
        static void Main(string[] args)
        {
            #region Interface

            ISaveableDocument document = new BillingDocument(15);
            //SaveTheDocument(document);
            //document.Amount = 150; //<- error
            //Console.WriteLine(document.Amount);
            //Implicit interface

            //Zadanie - stworzyć interfejsy reprezentujące konto w banku.
            //Jakie właściwości będą udostępniane na zewnątrz i w jaki sposób? zarówno get/set?
            //czy może tylko pojedyncze?
            //
            //Zadanie - zmodyfikować interfejs IBillingDocument tak aby można było zmienić kwotę dokumentu poprzez interfejs
            #endregion

            #region kolekcje
            BillingDocument[] array = new BillingDocument[15];
            System.Collections.ArrayList lista = new ArrayList();
            lista.Add(document); //<-brak sprawdzania typów
            lista.Add(document);
            lista.Add(5);

            foreach (BillingDocument bd in lista)
            {
                Console.WriteLine(bd);
            }
            //Zadanie - dodać do lista obiekt innego typu? Jaki będzie efekt kompilacji a jaki wykonania?
            //Zadanie - przerobić program tworzący tablicę ExchangeRate tak aby korzystał z ArrayList
            #endregion

            #region generics - typy generyczne

            List<BillingDocument> listInts = new List<BillingDocument>();
            listInts.Add(new BillingDocument(15));


            List<double> listDoubles = new List<double>();
            listDoubles.Add(1.5);
            listDoubles.Add(5); //<-- odkomentować - co tutaj się stało?
            //listDoubles.Add(document); //<-- odokmentować
            Dictionary<string, IBillingDocument> documents = new Dictionary<string, IBillingDocument>();
            documents.Add("Faktura", document);
            IBillingDocument doc2 = documents["Faktura"];
            System.Collections.Generic.Stack<int> stosIntow = new Stack<int>();
            stosIntow.Push(15);

            List<Dictionary<string, List<int>>> a = new List<Dictionary<string, List<int>>>();

            GenericDocument<BillingDocument> receipt = new GenericDocument<BillingDocument>();
            receipt.Issue(new BillingDocument());
            //GenericDocument<int> doc = new GenericDocument<int>(); //<-- odkomentować - co tutaj się stało?
            
            // Zadanie - stwórz własny typ generyczny reprezentujący dokument księgowy
            // Zadanie - przerobić aplikację czytającą ExchangeRate tak aby korzystał z kolekcji generycznej
            #endregion

            #region inferencja typów
            List<int> l = new List<int>(); // <-- duplikacja; to samo po prawej co po lewej
            var l2 = new List<int> {1, 2, 3}; // <-- interencja typów, kompilator sam wykryje typ
            IBillingDocument l3 = new BillingDocument(15);
            // to nie jest to samo co typ dynamiczny; wciąż mamy kontrolę typów

            //Zadanie: sprawdzić gdzie można użyc var:
            //- metoda? -
            //- parametry metody? NO
            //- zwracany wynik z metody? NO
            //- pola w klasie? No
            //- const? No
            //- static? NO
            //- właściwości w klasie? No

            //- przypisanie wyniku zwracanego z metody? No/Yes?
            //- foreach? Yes
            //- generics? No
            var doc = SaveTheDocument(document);
            
            #endregion
        }


























        public static ISaveableDocument SaveTheDocument(ISaveableDocument document)
        {
            string fileName = "NewFile.doc";
            document.Save(fileName);
            return document;
        }
    }
}
