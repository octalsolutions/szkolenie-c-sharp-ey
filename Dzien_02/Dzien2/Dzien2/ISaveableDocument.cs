﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dzien2
{
    interface ISaveableDocument
    {
        void Save(string fileName);

    }
}
