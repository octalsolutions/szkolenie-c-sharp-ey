﻿namespace Dzien2
{
    class BillingDocument : IBillingDocument, ISaveableDocument
    {
        public BillingDocument()
        {

        }
        public BillingDocument(decimal amount)
        {
            Amount = amount;
        }
        public int Type { get; set; }
        public decimal Amount { get; set; }

        public void SetAmount(decimal amount)
        {
            throw new System.NotImplementedException();
        }

        public void Issue()
        {
            
        }

        public void Save(string fileName)
        {
            
        }
    }
}
