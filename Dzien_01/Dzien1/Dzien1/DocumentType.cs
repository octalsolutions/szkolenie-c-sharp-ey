﻿using System;

namespace Dzien1_1
{
    [Flags]
    public enum MessageBoxButtons
    {
        Yes = 1,
        No = 2,
        Cancel = 4,
    }
    public enum DocumentType
    {
        Invoice = 55,
        Receipt = 80,
        DocB = 120
    }
}
