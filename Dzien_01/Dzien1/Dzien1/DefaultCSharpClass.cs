﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Conhost=System.Console;

namespace NazwaFirmy.Projekt.Domain
{
    public class DefaultCSharpClass
    {
        public DefaultCSharpClass() : base(16)
        {
            Conhost.WriteLine("Obiekt utworzony");
            base.Add("Element1");
        }

        ~DefaultCSharpClass()
        {

        }

        //Dopuszczalne: private, protected, internal, public, protected internal
        private string _firstProperty;
        public string FirstProperty { get; private set; }

        private List<int> field;
        public List<int> FieldProperty
        {
            get { return field; }
            set { field = value; }
        }



        public void Method(int argument1, string argument2)
        {
            this.Add("Element1");
            //using => try-finally
            using (var file = File.OpenRead(@"c:\temp\log"))
            {
                byte[] buf = new byte[1024];
                file.Read(buf, 0, 1024);
                Conhost.WriteLine("Using aliases!");
            } //==

            //Stream file = null;
            //try
            //{
            //    file = File.OpenRead(@"c:\temp\log");
            //byte[] buf = new byte[1024];
            //file.Read(buf, 0, 1024);
            //Conhost.WriteLine("Using aliases!");
            //}
            //finally
            //{
            //    if (file != null)
            //        file.Dispose();
            //}
        }

        public int Function(string arg2)
        {
            return 0;
        }
    }
}
