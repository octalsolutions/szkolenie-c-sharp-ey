﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dzien1_1
{

    class Program2
    {
        public void Main()
        {
            string info = "To jest napis";
            Console.WriteLine(string.IsNullOrEmpty(info) ? "Empty" : "Non-Empty");

            string info2 = "To jest napis2";
            Console.WriteLine(info2.IsNullOrEmpty() ? "Empty" : "Non-Empty");
        }
    }
    static class ExtensionMethods
    {
        public static bool IsNullOrEmpty(this string input)
        {
            return string.IsNullOrEmpty(input);
        }
    }
}
