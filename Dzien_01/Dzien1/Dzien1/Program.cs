﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Xml;

namespace Dzien1_1
{
    class Program
    {
        static async Task Main(string[] args)
        {
            //var populationByCountry = new PopulationByCountry();
            //var populationInPoland = populationByCountry["Poland", 1980];
            //Console.WriteLine(populationInPoland);
            //populationByCountry["UK", 2010] = 80000000;
            //var uk = populationByCountry["UK", 2010];
            //Console.WriteLine(uk);
            //DocumentType type = DocumentType.Invoice;
            //MessageBoxButtons buttons = MessageBoxButtons.Yes | MessageBoxButtons.No;

            //if ((buttons & MessageBoxButtons.Yes) == MessageBoxButtons.Yes)
            //{
            //    Console.WriteLine("Button Yes");
            //}

            //if ((buttons & MessageBoxButtons.No) == MessageBoxButtons.No)
            //{
            //    Console.WriteLine("Button No");
            //}

            //Console.WriteLine(buttons);
            #region Typy proste

            //int l = 5;
            //Console.WriteLine("Liczba = {0}", 5);
            //double d = 1.2;
            //float f = 1.1f;
            //decimal dm = 12M;
            //int vat = 23;
            //int vat2 = vat;
            //vat = 24;
            //Console.WriteLine("Vat2: {0}, vat: {1}", vat2, vat);

            ////-----
            //VatStruct vs1 = new VatStruct {Value = 5};
            //VatStruct vs2 = vs1;
            //vs1.Value = 8;
            //Console.WriteLine("Vat2: {0}, Vat: {1}", vs2.Value, vs1.Value);
            //Console.ReadLine();
            ////-----
            //ConsoleColor color = ConsoleColor.Magenta;
            //ConsoleColor color2 = color;
            //color = ConsoleColor.Cyan;
            //Console.WriteLine("Color2: {0}, color: {1}", color2, color);
            //#region typy proste - lista
            // Liczbowe: int, double, float
            // struktury: np. Point
            // enumy: np. ConsoleColor
            //#endregion
            #endregion

            //Console.WriteLine("\nNaciśnij Enter\n");
            //Console.ReadLine();
            //#region Typy referencyjne

            //VatClass v = new VatClass {Value = 23};
            //Console.WriteLine("VatClass: {0}", v.Value);
            //VatClass v2 = v;
            //v.Value = 24;
            //Console.WriteLine("Vat2: {0}, VatClass: {1}", v2.Value, v.Value);
            //Console.ReadLine();
            //#region Typy referencyjne - lista

            //string s = ".bak".ToUpper();
            // wszystko dziedziczące z Object w tym string! Ale string jest immutable!
            // klasy w tym nasze klasy czyli np. powyższy program (klasa Program)
            // Typy proste w momencie skorzystania z metod są pakowane w Object tzw. boxing -> kosztowne
            //#endregion
            //#endregion

            //Console.ReadLine();

            #region Zadanie
            // Zaproponuj zestaw obiektów, która będzie reprezentować rodzaje dokumentów księgowych?
            // Gdzie zastosujemy klasy a gdzie struktury?
            // Zadanie zaproponuj model obiektowy reprezentujący dane pochodzące z NBP: https://www.nbp.pl/kursy/xml/a194z181005.xml

            #endregion

            //
            //<pozycja>
            //<nazwa_waluty>bat (Tajlandia)</nazwa_waluty>
            //<przelicznik>1</przelicznik>
            //<kod_waluty>THB</kod_waluty>
            //<kurs_sredni>0,1141</kurs_sredni>
            //</pozycja>
            //ExchangeRate[] exchangeRates = new ExchangeRate[15];
            var t = CreateExchangeRatesAsync();

            Console.WriteLine("Reading exchange rates: ");
            var exchangeRates = await t;
            //Console.WriteLine(exchangeRates);
            //Predicate<ExchangeRate<string>> filter = ex => true;
            //Predicate<ExchangeRate<string>> filter = ex => ex.Rate > 1.0m;
            //Predicate<ExchangeRate<string>> filter = er => er.Rate > 1.0m && er.CurrencyCode.StartsWith("A");
            //foreach (var exchangeRate in exchangeRates)
            //{
            //    if (filter(exchangeRate))
            //        Console.WriteLine(exchangeRate);
            //}
            //var filteredExchangedRates = exchangeRates.Where(er => er.Rate > 1.0m && er.CurrencyName.Contains("a"));

            //var count = filteredExchangedRates.Count();
            //Console.WriteLine($"Count: {count}");

            //var sum = filteredExchangedRates.Sum(x => x.Rate);
            //Console.WriteLine($"Sum: {sum}");

            //Array.ForEach(filteredExchangedRates.OrderByDescending(x=>x.CurrencyCode).ToArray(), Console.WriteLine);
            //foreach (var filteredExchangedRate in filteredExchangedRates)
            //{
            //    Console.WriteLine(filteredExchangedRate);
            //}

            //Console.ReadLine();
            //int l = 5;
            //M(l);
            //float f = 500.0f;
            //N((int)f);
            //Console.WriteLine($"{0xf4}");

            //ExchangeRate ex = new ExchangeRate("Polish Zloty", 1, CurrencyCode.PLN, 1.0M);

            //DateTime dt = DateTime.Now;
            //object o = dt;
            //object obj = null;

            //obj = (ConsoleColor) o;

            //object obj1 = ex as BaseClass;
            //if (obj1 != null)
            //{

            //}
            Console.ReadLine();
            Task.WaitAll(t);
        }

        private static async Task<List<ExchangeRate<string>>> CreateExchangeRatesAsync()
        {
            var exchangeRates = new List<ExchangeRate<string>>();
            XmlReader r = XmlReader.Create(@"https://www.nbp.pl/kursy/xml/a194z181005.xml",
                new XmlReaderSettings {Async = true});
            //while (true)
            //{
            //    try
            //    {
            //        r = 
            //        break;
            //    }
            //    catch (WebException e)
            //    {
            //        Console.WriteLine("Brak połączenia z internetem. Ponowna próba za 3 sek.");
            //        await Task.Delay(3000);
            //    }
            //}

            using (r)

            {
                while (await r.ReadAsync())
                {
                    if (r.Name == "pozycja" && r.IsStartElement())
                    {
                        r.ReadToFollowing("nazwa_waluty");
                        var currencyName = r.ReadElementContentAsStringAsync();
                        r.ReadToFollowing("przelicznik");
                        var multiplier = r.ReadElementContentAsObjectAsync();
                        r.ReadToFollowing("kod_waluty");
                        var currencyType = r.ReadElementContentAsStringAsync();
                        r.ReadToFollowing("kurs_sredni");
                        decimal rate = decimal.Parse((await r.ReadElementContentAsObjectAsync()).ToString());

                        ExchangeRate<string> ex = new ExchangeRate<string>(await currencyName, int.Parse((await multiplier).ToString()), await currencyType, rate);
                        exchangeRates.Add(ex);
                    }
                }
            }

            return exchangeRates;
        }

        static bool FilterExchangeRatesThatStartsWithA(ExchangeRate<string> ex)
        {
            return ex.CurrencyName.StartsWith("A");
        }

        static bool FilterExchangeRatesThatRateIsAbove1(ExchangeRate<string> ex)
        {
            return ex.Rate > 1;
        }

        static bool NoFilter(ExchangeRate<string> ex)
        {
            return true;
        }

        static bool FilterExchangeRatesThatNameStartsWithBAndRateIsLowerThan1(ExchangeRate<string> ex)
        {
            return ex.CurrencyCode.StartsWith("B") && ex.Rate < 1;
        }

        //delegate bool Filter(ExchangeRate<string> ex);

        static void M(float i)
        {
            Console.WriteLine($"{i:F2}");
        }

        static void N(int i)
        {
            Console.WriteLine($"{i:F2}");
        }
    }

    class BaseClass
    {

    }

    enum CurrencyCode
    {
        THB,
        PLN,
        EUR,
        USD,
        AUD,
        HKD,
        CAD,
        NZD,
        SGD,
        HUF,
        CHF
    }

    class ExchangeRate<T>

    {
        public ExchangeRate(string currencyName, int multiplier, T code, decimal rate)
        {
            //if (rate < 0.1m)
            //    throw new VeryLowRateException(currencyName);

            CurrencyName = currencyName;
            Multiplier = multiplier;
            CurrencyCode = code;
            Rate = rate;
        }
        public string CurrencyName { get; private set; }
        public int Multiplier { get; private set; }
        public T CurrencyCode { get; private set; }
        public decimal Rate { get; private set; }

        public override string ToString()
        {
            return string.Format("Waluta: {0}, Kod waluty: {1}, Kurs wymiany: {2:F3}, Multiplier: {3}", CurrencyName, CurrencyCode, Rate, Multiplier);
        }
    }

    #region NotImportant
    class VatClass
    {
        public int Value { get; set; }
    }

    struct VatStruct
    {
        public int Value { get; set; }
    }
    #endregion
}
