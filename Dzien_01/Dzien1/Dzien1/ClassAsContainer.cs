﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dzien1_1
{
    public class PopulationByCountry
    {
        private Dictionary<string, Dictionary<int, decimal>> _populationByCountry;

        public PopulationByCountry()
        {
            _populationByCountry = new Dictionary<string, Dictionary<int, decimal>>();
            _populationByCountry.Add(
                "Poland", new Dictionary<int, decimal>
                {
                    {1980, 48000000m}
                });
        }

        public decimal this[string key, int key2]
        {
            get { return _populationByCountry[key][key2]; }
            set
            {
                if (_populationByCountry.ContainsKey(key) == false)
                {
                    _populationByCountry.Add(key, new Dictionary<int, decimal>());
                }

                _populationByCountry[key].Add(key2, value);
            }
        }

        //public decimal this[int key]
        //{
        //    get { return _populationByCountry[key]; }
        //}
    }
}
