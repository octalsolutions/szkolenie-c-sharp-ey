﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dzien1_1
{
    class VeryLowRateException : Exception
    {
        public VeryLowRateException()
        {
        }

        public VeryLowRateException(string message) : base(message)
        {
        }

        public VeryLowRateException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected VeryLowRateException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
