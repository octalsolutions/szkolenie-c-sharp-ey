﻿using System;

namespace Dzien1_2
{
    class Program
    {
        static void Main(string[] args)
        {
            //as value, ref, out
            VatStruct services;
            //Console.WriteLine("Vat na usługi: {0}", services.Value);
            //UpdateVat(out services);
            //Console.WriteLine("Vat na usługi: {0}", services.Value);

            //int v = int.Parse("aaaa");

            //Console.WriteLine($"{v}");
            UpdateVat(inputs: new int[] {3, 5, 7}, b: 12);
            UpdateVat(1.1, document: new Invoice(), inputs: new int[] {1, 2, 3, 4, 5});

            Console.ReadLine();
        }

        private static void UpdateVat(double b, int[] inputs)
        {
            UpdateVat(b, inputs, document: new EmptyDocument());
        }
        private static void UpdateVat(double b, int[] inputs, Document document)
        {
            document.Issue();
            foreach (int input in inputs)
            {
                Console.WriteLine(input);
            }
        }
    }

    internal sealed class EmptyDocument : Document
    {
        public override void Issue()
        {
            //do nothing on purpose
        }
    }

    class Invoice : Document

    {
        public override void Issue()
        {
            ///
        }
    }

    abstract class Document
    {
        public abstract void Issue();
    }

    struct VatStruct
    {
        public int Value { get; set; }
    }
}
