﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFDemo
{
    public partial class Agenda
    {
        public Agenda(int id, string title, string description)
        {
            this.Id = id;
            this.Title = title;
            this.Description = description;
        }
    }
}
