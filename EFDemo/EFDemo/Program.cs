﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            var testEntities = new TestEntities();
            testEntities.Database.Log = Console.WriteLine;

            testEntities.Agenda.Add(new Agenda(11,"Desc", "Nowy wpis z EF");
            //testEntities.SaveChanges();
            //var itemNo5 = testEntities.Agenda.First(x => x.Id == 5);
            //itemNo5.Title = "Nowy tytuł";
            testEntities.Agenda.Remove(testEntities.Agenda.First());

            testEntities.SaveChanges();
            var filteredEntries = testEntities.Agenda.Where(x => x.Id > 6);
            foreach (var item in filteredEntries)
            {
                Console.WriteLine(
                    $"Id: {item.Id}, title: {item.Title}, description: {item.Description}");
            }
        }
    }
}
