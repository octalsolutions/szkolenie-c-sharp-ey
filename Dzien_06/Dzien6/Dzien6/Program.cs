﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dzien6
{
    class Program
    {
        static void Main(string[] args)
        {
            //Źródło: https://www.connectionstrings.com/
            var connectionString = ConfigurationManager.ConnectionStrings["MyConnectionString"].ConnectionString;
            using (SqlConnection connection =
                new SqlConnection(connectionString))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();

                using (var command = new SqlCommand("INSERT INTO Agenda(Title, Description) VALUES(@title, @description)", connection))
                {
                    command.Parameters.AddWithValue("@title", "Dzien 2");
                    command.Parameters.AddWithValue("@Description", "Nic ciekawego.");
                    command.ExecuteNonQuery();
                }

                using (var command = new SqlCommand("select count(*) from Agenda", connection))
                {
                    var count = (int)command.ExecuteScalar();
                    Console.WriteLine($"Elementów w tabeli: {count}");
                    //command.
                    //int idIndex = reader.GetOrdinal("Id");
                    //int descIndex = reader.GetOrdinal("Description");
                    //while (reader.Read())
                    //{

                    //    Console.WriteLine("Id: {0}, Description: {1}", reader.GetInt32(idIndex), 
                    //                                                   reader.GetString(descIndex));
                    //}
                }
            }
            //Zadanie: jak umieścić connection string poza kodem? Dlaczego warto
            //Zadanie: napisz kod, który doda nowy wiersz z danymi do tabeli Agenda
            //Zadanie: zamodeluj bazę danych dla ExchangeRates i napisz kod, który dodaje je do tabeli
        }
    }
}
