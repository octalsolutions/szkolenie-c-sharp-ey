﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace Dzien03
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Typy anonimowe

            var anonim = new {Imie = "Paweł", Nazwisko = "Łukasik", Wiek = 20};
            Console.WriteLine(anonim.Imie);

            var anonim2 = new {Imie = "Paweł", Wiek = 20, Nazwisko = "Łukasik"};
            Console.WriteLine(anonim2.Nazwisko);

            var tab = new [] {new {Imie = "Paweł"}, new {Imie = "Tomasz"}};
            var item = new {FullName = new {FirstName = "Paweł", LastName = "Łukasik"}, Document = new BillingDocument()};
            //Zagnieżdzone typy anonimowe

            Console.WriteLine(anonim.Equals(anonim2));
            //CantUseAnonymousTypesInMethods(anonim, anonim2);
            Console.ReadLine();
            // Zadanie: jak przekazać typ anonimowy do funkcji? Czy jest to możliwe?
            // Zadanie: jakie zastosowania widzisz dla typów anonimowych?
            // Zadanie: Spróbuj utworzyć tablicę typów anonimowych
            // Zadanie: Przerób program wczytujący ExchangeRates tak aby korzystał z typów anonimowych
            #endregion

            Console.ReadLine();
            
            #region Typ dynamic

            object o = 12;
            //o.SuperMetoda(); //<-- odkomentować. Jaki jest rezultat?
            dynamic p = 15;
            //p.SuperMetoda(); //<-- odkomentować. Czemu to się kompiluje?
            //CantUseAnonymousTypesInMethods(new BillingDocument(), p);
            var json =
                "{\r\n    \"glossary\": {\r\n        \"title\": \"example glossary\",\r\n\t\t\"GlossDiv\": {\r\n            \"title\": \"S\",\r\n\t\t\t\"GlossList\": {\r\n                \"GlossEntry\": {\r\n                    \"ID\": \"SGML\",\r\n\t\t\t\t\t\"SortAs\": \"SGML\",\r\n\t\t\t\t\t\"GlossTerm\": \"Standard Generalized Markup Language\",\r\n\t\t\t\t\t\"Acronym\": \"SGML\",\r\n\t\t\t\t\t\"Abbrev\": \"ISO 8879:1986\",\r\n\t\t\t\t\t\"GlossDef\": {\r\n                        \"para\": \"A meta-markup language, used to create markup languages such as DocBook.\",\r\n\t\t\t\t\t\t\"GlossSeeAlso\": [\"GML\", \"XML\"]\r\n                    },\r\n\t\t\t\t\t\"GlossSee\": \"markup\"\r\n                }\r\n            }\r\n        }\r\n    }\r\n}";
            dynamic data = JsonConvert.DeserializeObject(json);
            //Console.WriteLine(data.glossary.GlossDiv.title);
            dynamic x = 5;
            dynamic y = 10;
            dynamic z = "15";
            Console.WriteLine(x + y + z);
            // Zadanie: Odkomentować powyższe linie. Dlaczego powyższy kod zachowuje się tak a nie inaczej?
            // Zadanie: Przerobić powyższy przykład z typami anonimowymi na typ dynamic 
            #endregion

            Console.ReadLine();

            #region Delegaty

            Func<int,int,int> doOp = null;
            Console.WriteLine("Podaj operację: +/-");
            string choice = Console.ReadLine();
            if (choice == "-")
                doOp = (a, b) => a - b;
            else if (choice == "+")
                doOp = (a, b) =>
                {
                    Console.WriteLine("using lambda");
                    return a + b;
                };
            else
                doOp = (_,__) => 0;

            WriteToSth writer = s => { };

            DoTheCalculations(doOp, 15, 60, writer);

            //Zadanie: zaimplementuj kalkulator wczytujący operacje oraz argumenty z konsoli korzystający z delegatów
            //Zadanie: Zaimplementuj opcje filtrowania i dodaj ją do kodu wczytującego ExchangeRates aby umożliwić wypisywanie tylko niektórych ExchangeRate'ów
            #region Anonimowe delegaty

            doOp = delegate(int a, int b) { return a / b; };
            writer = delegate { };
            DoTheCalculations(doOp, 15, 60, writer);
            //Console.WriteLine(doOp(15, 0));

            //Zadanie: to co powyżej za pomocą anonimowych delegatów :)
            #endregion

            #endregion
        }

        static void EmptyPrinter(string s)
        {
            //empty on purpose
        }

        static void DoTheCalculations(Func<int, int, int> op, int a, int b, WriteToSth writer)
        {
            writer($"Wykonuję operację dla a={a} i b={b}");
            var wynik = op(a, b);
            writer($"Wynik to: {wynik}");
            
        }

        static void CantUseAnonymousTypesInMethods(dynamic anonim1, dynamic anonim2)
        {
            Console.WriteLine(anonim1.SuperMetoda());
            Console.WriteLine(anonim2.SuperMetoda());
            Console.WriteLine(anonim1.ToString());
        }

        //delegate int Calculations(int a, int b);

        //delegate double CalculateDouble(double a, double b);

        delegate void WriteToSth(string text);

        //static int Nothing(int z, int y)
        //{
        //    Console.WriteLine("Nie rozumiem tej operacji. ;(");
        //    return 0;
        //}
        //static int Addition(int a, int b)
        //{
        //    return a + b;
        //}

        //static int Subtraction(int a, int b)
        //{
        //    return a - b;
        //}
    }
}
