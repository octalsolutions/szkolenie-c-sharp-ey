﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class DerivedClass : AbstractClass
    {
        public override void A()
        {
            if (C == 15)
            {
                base.A();
            }

            Console.WriteLine("DerivedClass:A");
        }

        public new void B()
        {
            Console.WriteLine("DerivedClass:B");
        }
    }
}
