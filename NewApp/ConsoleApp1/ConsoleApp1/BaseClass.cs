﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{

    public abstract class AbstractClass
    {
        public abstract void A();


    }
    public sealed class BaseClass
    {
        public static int C { get; set; }
        public int D { get; set; }
        public virtual void A()
        {
            Console.WriteLine("BaseClass:A");
        }

        public static void E()
        {
            C = 20;
            D = 11;
        }

        public void B()
        {
            Console.WriteLine("BaseClass:B");
        }
    }
}
