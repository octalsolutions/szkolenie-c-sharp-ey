﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            BaseClass baseClass = new BaseClass();
            baseClass.A();
            baseClass.B();
            BaseClass derivedClass = new DerivedClass();
            derivedClass.A();
            derivedClass.B();
        }
    }
}
