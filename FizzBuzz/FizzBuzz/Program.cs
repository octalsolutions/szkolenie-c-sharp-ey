﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz
{
    class Program
    {
        static void Main(string[] args)
        {
            var fizzBuzzGenerator = new FizzBuzzGenerator();
            //fizzBuzzGenerator.Fizz += FizzBuzzGenerator_Fizz;
            fizzBuzzGenerator.NormalCase += FizzBuzzGeneratorOnNormalCase;
            //fizzBuzzGenerator.Buzz += FizzBuzzGeneratorOnBuzz;
            fizzBuzzGenerator.Start();

            fizzBuzzGenerator.Fizz -= FizzBuzzGenerator_Fizz;
            fizzBuzzGenerator.NormalCase -= FizzBuzzGeneratorOnNormalCase;
            fizzBuzzGenerator.Buzz -= FizzBuzzGeneratorOnBuzz;
        }

        private static void FizzBuzzGeneratorOnBuzz(object sender, int e)
        {
            Console.WriteLine($"Liczba {e} jest Buzz");
        }

        private static void FizzBuzzGeneratorOnNormalCase(object sender, int e)
        {
            Console.WriteLine($"Liczba {e}");
        }

        private static void FizzBuzzGenerator_Fizz(object sender, int e)
        {
            Console.WriteLine($"Liczba {e} to Fizz!");
        }
    }
}
