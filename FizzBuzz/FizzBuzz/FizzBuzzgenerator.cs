﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz
{
    class FizzBuzzGenerator
    {
        public event EventHandler<int> Fizz;
        public event EventHandler<int> Buzz;
        public event EventHandler<int> NormalCase; 
        public void Start()
        {
            foreach (var i in Enumerable.Range(1,100))
            {
                if (i % 3 == 0 && i % 5 == 0)
                {
                    //FizzBuzz
                    Fizz?.Invoke(this,i);
                    Buzz?.Invoke(this,i);
                }
                else if (i % 5 == 0)
                {
                    //Buzz
                    Buzz?.Invoke(this, i);
                }
                else if (i % 3 == 0)
                {
                    //Fizz
                    Fizz?.Invoke(this, i);
                }
                else
                {
                    NormalCase?.Invoke(this, i);
                }
            }
        }
    }
}
