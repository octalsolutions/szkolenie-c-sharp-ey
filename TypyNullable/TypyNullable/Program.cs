﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypyNullable
{
    class Program
    {
        static void Main(string[] args)
        {
            bool? cont = true;
            while (cont ?? false) // 
            {
                Console.WriteLine("Podaj a:");
                int a = int.Parse(Console.ReadLine());
                Console.WriteLine("Podaj b:");
                int b = int.Parse(Console.ReadLine());
                int? result = Divide(a, b);
                Console.WriteLine(result.GetValueOrDefault(-1));

                Console.WriteLine("Czy chcesz kontynuować? T/N");
                string choice = Console.ReadLine();
                if (choice == "N")
                {
                    cont = false;
                }
                else if (choice == "T")
                {
                }
                else
                {
                    cont = null;
                }
            }

            //if (result.HasValue)
            //{
            //    Console.WriteLine(result.Value);
            //}
            //else
            //{
            //    Console.WriteLine("Sorry nie wiem co wypisać!");
            //}
        }

        static int? Divide(int? a, int? b)
        {
            if (b == 0) return null;
            return a / b;
        }
    }

    struct OperationResult<T> 
    {
        public T Result { get; }
        public bool HasError { get; } 
    }
}
